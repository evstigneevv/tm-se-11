package ru.evstigneev.tm;

import org.apache.log4j.BasicConfigurator;
import org.jetbrains.annotations.NotNull;
import ru.evstigneev.tm.api.bootstrap.ServiceLocator;
import ru.evstigneev.tm.bootstrap.Bootstrap;

public class Client {

    public static void main(String[] args) {
        BasicConfigurator.configure();
        @NotNull final ServiceLocator bootstrap = new Bootstrap();
        try {
            bootstrap.init();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}