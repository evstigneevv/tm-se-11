package ru.evstigneev.tm.api.bootstrap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.evstigneev.tm.api.endpoint.*;
import ru.evstigneev.tm.command.AbstractCommand;

import java.lang.Exception;
import java.util.Collection;
import java.util.Scanner;

public interface ServiceLocator {

    IUserEndpoint getUserEndpoint();

    void setUserEndpoint(@NotNull final IUserEndpoint userEndpoint);

    IProjectEndpoint getProjectEndpoint();

    void setProjectEndpoint(@NotNull final IProjectEndpoint projectEndpoint);

    ITaskEndpoint getTaskEndpoint();

    void setTaskEndpoint(@NotNull final ITaskEndpoint taskEndpoint);

    ISessionEndpoint getSessionEndpoint();

    void setSessionEndpoint(@NotNull final ISessionEndpoint sessionEndpoint);

    Scanner getScanner();

    Collection<AbstractCommand> getCommands();

    void init() throws Exception;

    Session getSession();

    void setSession(@Nullable final Session currentSession);

    IDomainEndpoint getDomainEndpoint();

    void setDomainEndpoint(@Nullable final IDomainEndpoint domainEndpoint);

}
