package ru.evstigneev.tm.command.data;

import ru.evstigneev.tm.command.AbstractCommand;

public class DataReadJacksonXmlCommand extends AbstractCommand {

    @Override
    public String command() {
        return "DATA READ JACKSON XML";
    }

    @Override
    public String description() {
        return "Read data .xml file with Jackson";
    }

    @Override
    public void execute() throws Exception {
        bootstrap.getDomainEndpoint().readDataJacksonXml(bootstrap.getSession());
    }

}
