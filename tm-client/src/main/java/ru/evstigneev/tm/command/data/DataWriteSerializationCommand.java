package ru.evstigneev.tm.command.data;

import ru.evstigneev.tm.command.AbstractCommand;

public class DataWriteSerializationCommand extends AbstractCommand {

    @Override
    public String command() {
        return "DATA WRITE";
    }

    @Override
    public String description() {
        return "Saving current data to file";
    }

    @Override
    public void execute() throws Exception {
        bootstrap.getDomainEndpoint().writeDataSerialization(bootstrap.getSession());
    }

}
