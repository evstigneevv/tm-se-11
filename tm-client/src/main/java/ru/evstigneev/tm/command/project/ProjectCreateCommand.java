package ru.evstigneev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.evstigneev.tm.command.AbstractCommand;

public class ProjectCreateCommand extends AbstractCommand {

    @Override
    public String command() {
        return "CP";
    }

    @Override
    public String description() {
        return "Create a new project";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("CREATE NEW PROJECT");
        System.out.println("Enter project name: ");
        @NotNull final String projectName = bootstrap.getScanner().nextLine();
        bootstrap.getProjectEndpoint().createProject(bootstrap.getSession(), projectName);
        System.out.println("project created!");
    }

}
