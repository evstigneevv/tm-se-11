package ru.evstigneev.tm.command.project;

import ru.evstigneev.tm.command.AbstractCommand;

public class ProjectDeleteAllCommand extends AbstractCommand {

    @Override
    public String command() {
        return "DAP";
    }

    @Override
    public String description() {
        return "Delete all projects!";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("DELETE ALL PROJECTS");
        bootstrap.getProjectEndpoint().removeAllProjects(bootstrap.getSession());
        System.out.println("All projects deleted!");
    }

}
