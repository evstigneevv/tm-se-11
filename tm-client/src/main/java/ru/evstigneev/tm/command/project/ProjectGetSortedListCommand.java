package ru.evstigneev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.evstigneev.tm.api.endpoint.Project;
import ru.evstigneev.tm.command.AbstractCommand;

public class ProjectGetSortedListCommand extends AbstractCommand {

    @Override
    public String command() {
        return "PGSL";
    }

    @Override
    public String description() {
        return "Show sorted by you choice list of projects";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("PROJECT SORT BY VALUE");
        System.out.println("Enter by which value sort projects: ");
        @NotNull final String comparatorName = bootstrap.getScanner().nextLine();
        for (Project project : bootstrap.getProjectEndpoint().sortProjects(bootstrap.getSession(), comparatorName)) {
            System.out.println("User ID: " + project.getUserId() + " | Project ID: " + project.getId() + " | Project name: "
                    + project.getName() + " | Project description: " + project.getDescription() + " | Date of creation: "
                    + project.getDateOfCreation() + " | Date of start: " + project.getDateStart() + " | Date of finish: "
                    + project.getDateFinish() + " | Project status: " + project.getStatus());
        }
        System.out.println();
    }

}
