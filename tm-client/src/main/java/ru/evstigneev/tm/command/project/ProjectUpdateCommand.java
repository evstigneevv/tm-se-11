package ru.evstigneev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.evstigneev.tm.command.AbstractCommand;

public class ProjectUpdateCommand extends AbstractCommand {

    @Override
    public String command() {
        return "UP";
    }

    @Override
    public String description() {
        return "Update project";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("UPDATE PROJECT");
        System.out.println("input project ID");
        @NotNull final String projectId = bootstrap.getScanner().nextLine();
        System.out.println("input project new name");
        bootstrap.getProjectEndpoint().updateProject(bootstrap.getSession(), projectId,
                bootstrap.getScanner().nextLine());
        System.out.println();
    }

}
