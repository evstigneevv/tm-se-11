package ru.evstigneev.tm.command.system;

import ru.evstigneev.tm.command.AbstractCommand;

public class ExitCommand extends AbstractCommand {

    @Override
    public String command() {
        return "EXIT";
    }

    @Override
    public String description() {
        return "Closes application";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("EXIT");
        System.exit(2);
    }

}
