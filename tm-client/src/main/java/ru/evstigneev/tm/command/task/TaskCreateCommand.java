package ru.evstigneev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.evstigneev.tm.command.AbstractCommand;

public class TaskCreateCommand extends AbstractCommand {

    @Override
    public String command() {
        return "CT";
    }

    @Override
    public String description() {
        return "Create new task";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("CREATE NEW TASK");
        System.out.println("input project ID: ");
        @NotNull final String projectId = bootstrap.getScanner().nextLine();
        System.out.println("input new task name into project: ");
        bootstrap.getTaskEndpoint().createTask(bootstrap.getSession(), projectId, bootstrap.getScanner().nextLine());
        System.out.println("task created!");
    }

}
