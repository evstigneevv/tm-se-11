package ru.evstigneev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.evstigneev.tm.command.AbstractCommand;

public class TaskUpdateCommand extends AbstractCommand {

    @Override
    public String command() {
        return "TU";
    }

    @Override
    public String description() {
        return "Update task";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("enter task ID: ");
        @NotNull final String taskId = bootstrap.getScanner().nextLine();
        System.out.println("enter new task name: ");
        bootstrap.getTaskEndpoint().updateTask(bootstrap.getSession(), taskId, bootstrap.getScanner().nextLine());
        System.out.println("TASK NAME CHANGED");
        System.out.println();
    }

}
