package ru.evstigneev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.evstigneev.tm.api.endpoint.RoleType;
import ru.evstigneev.tm.command.AbstractCommand;

public class UserCreateCommand extends AbstractCommand {

    @Override
    public String command() {
        return "CU";
    }

    @Override
    public String description() {
        return "Creates new user";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("CREATE NEW USER");
        if (bootstrap.getUserEndpoint().isEmptyUserList()) {
            System.out.println("Enter new user login: ");
            @NotNull final String login = bootstrap.getScanner().nextLine();
            System.out.println("Enter new user password: ");
            @NotNull final String password = bootstrap.getScanner().nextLine();
            bootstrap.getUserEndpoint().createAdmin(login, password);
            bootstrap.setSession(bootstrap.getSessionEndpoint().openSession(login, password));
            System.out.println("User created!");
            return;
        }
        System.out.println("Enter new user login: ");
        @NotNull final String login = bootstrap.getScanner().nextLine();
        System.out.println("Enter new user password: ");
        @NotNull final String password = bootstrap.getScanner().nextLine();
        System.out.println("Enter new user role(admin or user): ");
        @NotNull final String roleType = bootstrap.getScanner().nextLine();
        bootstrap.getUserEndpoint().createUser(bootstrap.getSession(), login, password, RoleType.valueOf(roleType.toUpperCase()));
        System.out.println("User created!");
    }

}
