package ru.evstigneev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.evstigneev.tm.command.AbstractCommand;

public class UserUpdateCommand extends AbstractCommand {

    @Override
    public String command() {
        return "UU";
    }

    @Override
    public String description() {
        return "Update current user";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("UPDATE USER");
        System.out.println("enter new user name: ");
        @NotNull final String newName = bootstrap.getScanner().nextLine();
        bootstrap.getUserEndpoint().updateUser(bootstrap.getSession(), newName);
        System.out.println("User name has changed!");
    }

}

