package ru.evstigneev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.evstigneev.tm.command.AbstractCommand;

public class UserUpdatePasswordCommand extends AbstractCommand {

    @Override
    public String command() {
        return "UUP";
    }

    @Override
    public String description() {
        return "Update user password";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("UPDATE PASSWORD");
        System.out.println("enter old password: ");
        @NotNull final String oldPassword = bootstrap.getScanner().nextLine();
        if (bootstrap.getUserEndpoint().checkPasswordByUserId(bootstrap.getSession().getUserId(), oldPassword)) {
            System.out.println("enter new password: ");
            @NotNull final String newPassword = bootstrap.getScanner().nextLine();
            bootstrap.getUserEndpoint().updatePassword(bootstrap.getSession(), newPassword);
            System.out.println("password was changed!");
        }
    }

}
