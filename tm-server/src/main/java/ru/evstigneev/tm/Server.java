package ru.evstigneev.tm;

import org.apache.log4j.BasicConfigurator;
import org.jetbrains.annotations.NotNull;
import ru.evstigneev.tm.api.bootstrap.ServiceLocator;
import ru.evstigneev.tm.bootstrap.Bootstrap;

public class Server {

    public static void main(String[] args) {
        BasicConfigurator.configure();
        @NotNull final ServiceLocator bootstrap = new Bootstrap();
        bootstrap.init();
    }

}
