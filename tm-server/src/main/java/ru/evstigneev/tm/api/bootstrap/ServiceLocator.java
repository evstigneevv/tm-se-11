package ru.evstigneev.tm.api.bootstrap;

import org.jetbrains.annotations.NotNull;
import ru.evstigneev.tm.api.endpoint.*;
import ru.evstigneev.tm.api.repository.ITaskRepository;
import ru.evstigneev.tm.api.service.IProjectService;
import ru.evstigneev.tm.api.service.ITaskService;
import ru.evstigneev.tm.api.service.IUserService;
import ru.evstigneev.tm.util.DateParser;

import java.util.Scanner;

public interface ServiceLocator {

    @NotNull
    Scanner getScanner();

    @NotNull
    ITaskRepository getTaskRepository();

    @NotNull
    IProjectService getProjectService();

    @NotNull
    ITaskService getTaskService();

    @NotNull IUserService getUserService();

    @NotNull IUserEndpoint getUserEndpoint();

    void setUserEndpoint(@NotNull final IUserEndpoint userEndpoint);

    @NotNull IProjectEndpoint getProjectEndpoint();

    void setProjectEndpoint(@NotNull final IProjectEndpoint projectEndpoint);

    @NotNull ITaskEndpoint getTaskEndpoint();

    void setTaskEndpoint(@NotNull final ITaskEndpoint taskEndpoint);

    @NotNull DateParser getDateParser();

    @NotNull ISessionEndpoint getSessionEndpoint();

    void setSessionEndpoint(@NotNull final ISessionEndpoint sessionEndpoint);

    @NotNull IDomainEndpoint getDomainEndpoint();

    void setDomainEndpoint(@NotNull final IDomainEndpoint domainEndpoint);

    void init();

}
