package ru.evstigneev.tm.api.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.evstigneev.tm.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface IDomainEndpoint {

    String URL = "http://localhost:8080/DomainEndpoint?wsdl";

    @WebMethod
    void writeDataJacksonJson(@WebParam(name = "session") @Nullable final Session session) throws Exception;

    @WebMethod
    void readDataJacksonJson(@WebParam(name = "session") @Nullable final Session session) throws Exception;

    @WebMethod
    void readDataJacksonXml(@WebParam(name = "session") @Nullable final Session session) throws Exception;

    @WebMethod
    void writeDataJacksonXml(@WebParam(name = "session") @Nullable final Session session) throws Exception;

    @WebMethod
    void readDataJaxbXml(@WebParam(name = "session") @Nullable final Session session) throws Exception;

    @WebMethod
    void writeDataJaxbXml(@WebParam(name = "session") @Nullable final Session session) throws Exception;

    @WebMethod
    void readDataJaxbJson(@WebParam(name = "session") @Nullable final Session session) throws Exception;

    @WebMethod
    void writeDataJaxbJson(@WebParam(name = "session") @Nullable final Session session) throws Exception;

    @WebMethod
    void writeDataSerialization(@WebParam(name = "session") @Nullable final Session session) throws Exception;

    @WebMethod
    void readDataSerialization(@WebParam(name = "session") @Nullable final Session session) throws Exception;

}
