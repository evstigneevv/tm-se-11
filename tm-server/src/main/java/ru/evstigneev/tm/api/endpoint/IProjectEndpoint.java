package ru.evstigneev.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.evstigneev.tm.entity.Project;
import ru.evstigneev.tm.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;
import java.util.List;

@WebService
public interface IProjectEndpoint {

    String URL = "http://localhost:8080/ProjectEndpoint?wsdl";

    @WebMethod
    Collection<Project> findAllProjects(@WebParam(name = "session") @NotNull final Session session) throws Exception;

    @WebMethod
    Project createProject(@WebParam(name = "session") @NotNull final Session session,
                          @WebParam(name = "projectName") @NotNull final String projectName) throws Exception;

    @WebMethod
    boolean removeProject(@WebParam(name = "session") @NotNull final Session session,
                          @WebParam(name = "projectId") @NotNull final String projectId) throws Exception;

    @WebMethod
    Project updateProject(@WebParam(name = "session") @NotNull final Session session,
                          @WebParam(name = "projectId") @NotNull final String projectId,
                          @WebParam(name = "newProjectName") @NotNull final String newProjectName) throws Exception;

    @WebMethod
    Project findOneProject(@WebParam(name = "session") @NotNull final Session session,
                           @WebParam(name = "projectId") @NotNull final String projectId) throws Exception;

    @WebMethod
    Project mergeProject(@WebParam(name = "session") @NotNull final Session session,
                         @WebParam(name = "project") @NotNull final Project project) throws Exception;

    @WebMethod
    Project persistProject(@WebParam(name = "session") @NotNull final Session session,
                           @WebParam(name = "project") @NotNull final Project project) throws Exception;

    @WebMethod
    Collection<Project> findAllProjectsByUserId(@WebParam(name = "session") @NotNull final Session session) throws Exception;

    @WebMethod
    void removeAllProjects(@WebParam(name = "session") @NotNull final Session session) throws Exception;

    @WebMethod
    boolean removeAllProjectsByUserId(@WebParam(name = "session") @NotNull final Session session) throws Exception;

    @WebMethod
    List<Project> sortProjects(@WebParam(name = "session") @NotNull final Session session,
                               @WebParam(name = "comparatorName") @NotNull final String comparatorName) throws Exception;

    @WebMethod
    List<Project> searchProjectByString(@WebParam(name = "session") @NotNull final Session session,
                                        @WebParam(name = "string") @NotNull final String string) throws Exception;

}
