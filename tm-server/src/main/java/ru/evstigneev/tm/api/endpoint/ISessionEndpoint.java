package ru.evstigneev.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.evstigneev.tm.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;
import java.util.List;

@WebService
public interface ISessionEndpoint {

    String URL = "http://localhost:8080/SessionEndpoint?wsdl";

    @WebMethod
    void validate(@WebParam(name = "session") @NotNull final Session session) throws Exception;

    @WebMethod
    Session openSession(@WebParam(name = "login") @NotNull final String login,
                        @WebParam(name = "password") @NotNull final String password) throws Exception;

    @WebMethod
    boolean closeSession(@WebParam(name = "session") @NotNull final Session session) throws Exception;

    @WebMethod
    Session merge(@WebParam(name = "session") @NotNull final Session session) throws Exception;

    @WebMethod
    Session persist(@WebParam(name = "session") @NotNull final Session session) throws Exception;

    @WebMethod
    void removeAll(@WebParam(name = "session") @NotNull final Session session) throws Exception;

    @WebMethod
    boolean removeByUserId(@WebParam(name = "session") @NotNull final Session session) throws Exception;

    @WebMethod
    Session findSessionByUserId(@WebParam(name = "session") @NotNull final Session session) throws Exception;

    @WebMethod
    Collection<Session> findAll(@WebParam(name = "session") @NotNull final Session session) throws Exception;

    @WebMethod
    List<String> getBlockedSignatureList(@WebParam(name = "session") @NotNull final Session session) throws Exception;

    @WebMethod
    void setBlockedSignatureList(@WebParam(name = "session") @NotNull final Session session,
                                 @WebParam(name = "list") @NotNull final List<String> blockedSignatureList) throws Exception;

    @WebMethod
    boolean addSessionToBlockedList(@WebParam(name = "session") @NotNull final Session session,
                                    @WebParam(name = "signature") @NotNull final String signature) throws Exception;

}
