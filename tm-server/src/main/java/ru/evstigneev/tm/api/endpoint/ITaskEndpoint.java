package ru.evstigneev.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.evstigneev.tm.entity.Session;
import ru.evstigneev.tm.entity.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;
import java.util.List;

@WebService
public interface ITaskEndpoint {

    String URL = "http://localhost:8080/TaskEndpoint?wsdl";

    @WebMethod
    Task createTask(@WebParam(name = "session") @NotNull final Session session,
                    @WebParam(name = "projectId") @NotNull final String projectId,
                    @WebParam(name = "taskName") @NotNull final String taskName) throws Exception;

    @WebMethod
    Collection<Task> findAllTasks(@WebParam(name = "session") @NotNull final Session session) throws Exception;

    @WebMethod
    Collection<Task> findAllTasksByUserId(@WebParam(name = "session") @NotNull final Session session) throws Exception;

    @WebMethod
    boolean removeTask(@WebParam(name = "session") @NotNull final Session session,
                       @WebParam(name = "taskId") @NotNull final String taskId) throws Exception;

    @WebMethod
    Collection<Task> getTaskListByProjectId(@WebParam(name = "session") @NotNull final Session session,
                                            @WebParam(name = "projectId") @NotNull final String projectId) throws Exception;

    @WebMethod
    Task updateTask(@WebParam(name = "session") @NotNull final Session session,
                    @WebParam(name = "taskId") @NotNull final String taskId,
                    @WebParam(name = "taskName") @NotNull final String taskName) throws Exception;

    @WebMethod
    Task mergeTask(@WebParam(name = "session") @NotNull final Session session,
                   @WebParam(name = "task") @NotNull final Task task) throws Exception;

    @WebMethod
    Task persistTask(@WebParam(name = "session") @NotNull final Session session,
                     @WebParam(name = "task") @NotNull final Task task) throws Exception;

    @WebMethod
    void removeAllTasks(@WebParam(name = "session") @NotNull final Session session) throws Exception;

    @WebMethod
    boolean removeAllTasksByUserId(@WebParam(name = "session") @NotNull final Session session) throws Exception;

    @WebMethod
    boolean deleteAllProjectTasks(@WebParam(name = "session") @NotNull final Session session,
                                  @WebParam(name = "projectId") @NotNull final String projectId) throws Exception;

    @WebMethod
    Task findOne(@WebParam(name = "session") @NotNull final Session session,
                 @WebParam(name = "projectId") @NotNull final String projectId,
                 @WebParam(name = "taskId") @NotNull final String taskId) throws Exception;

    @WebMethod
    List<Task> sortTasks(@WebParam(name = "session") @NotNull final Session session,
                         @WebParam(name = "comparatorName") @NotNull final String comparatorName) throws Exception;

    @WebMethod
    List<Task> searchTaskByString(@WebParam(name = "session") @NotNull final Session session,
                                  @WebParam(name = "string") @NotNull final String string) throws Exception;

}
