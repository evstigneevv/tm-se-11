package ru.evstigneev.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.evstigneev.tm.entity.Session;
import ru.evstigneev.tm.entity.User;
import ru.evstigneev.tm.enumerated.RoleType;
import ru.evstigneev.tm.exception.EmptyStringException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;

@WebService
public interface IUserEndpoint {

    String URL = "http://localhost:8080/UserEndpoint?wsdl";

    @WebMethod
    User createAdmin(@WebParam(name = "login") @NotNull final String login,
                     @WebParam(name = "password") @NotNull final String password) throws EmptyStringException;

    @WebMethod
    User createUser(@WebParam(name = "session") @NotNull final Session session,
                    @WebParam(name = "login") @NotNull final String login,
                    @WebParam(name = "password") @NotNull final String password,
                    @WebParam(name = "role") @NotNull final RoleType role) throws Exception;

    @WebMethod
    Collection<User> findAllUsers(@WebParam(name = "session") @NotNull final Session session) throws Exception;

    @WebMethod
    boolean checkPassword(@WebParam(name = "login") @NotNull final String login,
                          @WebParam(name = "password") @NotNull final String password) throws Exception;

    @WebMethod
    boolean checkPasswordByUserId(@WebParam(name = "userId") @NotNull final String userId,
                                  @WebParam(name = "password") @NotNull final String password) throws Exception;

    @WebMethod
    boolean updatePassword(@WebParam(name = "session") @NotNull final Session session,
                           @WebParam(name = "newPassword") @NotNull final String newPassword) throws Exception;

    @WebMethod
    User findUserByLogin(@WebParam(name = "session") @NotNull final Session session,
                         @WebParam(name = "login") @NotNull final String login) throws Exception;

    @WebMethod
    void updateUser(@WebParam(name = "session") @NotNull final Session session,
                    @WebParam(name = "userName") @NotNull final String userName) throws Exception;

    @WebMethod
    boolean removeUser(@WebParam(name = "session") @NotNull final Session session,
                       @WebParam(name = "userId") @NotNull final String userId) throws Exception;

    @WebMethod
    User persistUser(@WebParam(name = "session") @NotNull final Session session,
                     @WebParam(name = "user") @NotNull final User user) throws Exception;

    @WebMethod
    void removeAllUsers(@WebParam(name = "session") @NotNull final Session session) throws Exception;

    @WebMethod
    User mergeUser(@WebParam(name = "session") @NotNull final Session session,
                   @WebParam(name = "user") @NotNull final User user) throws Exception;

    @WebMethod
    boolean isEmptyUserList();

}
