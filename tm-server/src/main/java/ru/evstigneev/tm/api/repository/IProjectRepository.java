package ru.evstigneev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.evstigneev.tm.entity.Project;

import java.util.Collection;

public interface IProjectRepository {

    Project create(@NotNull final String userId, @NotNull final String projectName);

    boolean remove(@NotNull final String userId, @NotNull final String projectId);

    Project update(@NotNull final String userId, @NotNull final String projectId, @NotNull final String projectName);

    Collection<Project> findAll() throws Exception;

    Collection<Project> findAllByUserId(@NotNull final String userId);

    Project findOne(@NotNull final String userId, @NotNull final String projectId);

    Project merge(@NotNull final String userId, @NotNull final Project project);

    Project persist(@NotNull final String userId, @NotNull final Project project);

    boolean removeAllByUserId(@NotNull final String userId);

    void removeAll();

}
