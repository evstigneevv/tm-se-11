package ru.evstigneev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.evstigneev.tm.entity.Session;
import ru.evstigneev.tm.exception.EmptyStringException;

import java.util.Collection;
import java.util.List;

public interface ISessionRepository {

    Session merge(@NotNull final String userId, @NotNull final Session session) throws EmptyStringException;

    Session persist(@NotNull final String userId, @NotNull final Session session) throws EmptyStringException;

    void removeAll();

    boolean removeByUserId(@NotNull final String userId);

    Session findSessionByUserId(@NotNull final String userId);

    Collection<Session> findAll();

    List<String> getBlockedSignatureList();

    void setBlockedSignatureList(@NotNull final List<String> blockedSignatureList);

    boolean addSessionToBlockedList(@NotNull final String signature);

    boolean addSession(@NotNull final String id, @NotNull final Session session);

}
