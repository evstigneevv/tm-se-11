package ru.evstigneev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.evstigneev.tm.entity.Session;
import ru.evstigneev.tm.enumerated.RoleType;
import ru.evstigneev.tm.exception.EmptyStringException;

import java.util.Collection;
import java.util.List;

public interface ISessionService {

    void validate(@NotNull final Session session) throws Exception;

    void validate(@Nullable final Session session, @NotNull final RoleType roleType) throws Exception;

    Session openSession(@NotNull final String login, @NotNull final String password) throws Exception;

    boolean closeSession(@NotNull final Session session);

    Session merge(@NotNull final String userId, @NotNull final Session session) throws EmptyStringException;

    Session persist(@NotNull final String userId, @NotNull final Session session) throws EmptyStringException;

    void removeAll();

    boolean removeByUserId(@NotNull final String userId);

    Session findSessionByUserId(@NotNull final String userId);

    Collection<Session> findAll();

    List<String> getBlockedSignatureList();

    void setBlockedSignatureList(@NotNull final List<String> blockedSignatureList);

    boolean addSessionToBlockedList(@NotNull final String signature);

}
