package ru.evstigneev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.evstigneev.tm.entity.User;
import ru.evstigneev.tm.enumerated.RoleType;
import ru.evstigneev.tm.exception.EmptyStringException;

import java.util.Collection;

public interface IUserService {

    User create(@NotNull final String login, @NotNull final String password) throws EmptyStringException;

    User create(@NotNull final String login, @NotNull final String password, @NotNull final RoleType role) throws EmptyStringException;

    Collection<User> findAll();

    boolean checkPassword(@NotNull final String login, @NotNull final String password) throws Exception;

    boolean checkPasswordByUserId(@NotNull final String userId, @NotNull final String password) throws Exception;

    boolean updatePassword(@NotNull final String userId, @NotNull final String newPassword) throws EmptyStringException;

    boolean isAdmin(@NotNull final User user);

    User findByLogin(@NotNull final String login) throws EmptyStringException;

    void update(@NotNull final String userId, @NotNull final String userName) throws EmptyStringException;

    boolean remove(@NotNull final String userId) throws EmptyStringException;

    User persist(@NotNull final String userId, @NotNull final User user) throws EmptyStringException;

    void removeAll();

    User merge(@NotNull final String userId, @NotNull final User user) throws EmptyStringException;

}
