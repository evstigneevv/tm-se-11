package ru.evstigneev.tm.bootstrap;

import org.jetbrains.annotations.NotNull;
import ru.evstigneev.tm.api.bootstrap.ServiceLocator;
import ru.evstigneev.tm.api.endpoint.*;
import ru.evstigneev.tm.api.repository.ITaskRepository;
import ru.evstigneev.tm.api.repository.IUserRepository;
import ru.evstigneev.tm.api.service.IProjectService;
import ru.evstigneev.tm.api.service.ISessionService;
import ru.evstigneev.tm.api.service.ITaskService;
import ru.evstigneev.tm.api.service.IUserService;
import ru.evstigneev.tm.endpoint.*;
import ru.evstigneev.tm.repository.ProjectRepository;
import ru.evstigneev.tm.repository.SessionRepository;
import ru.evstigneev.tm.repository.TaskRepository;
import ru.evstigneev.tm.repository.UserRepository;
import ru.evstigneev.tm.service.ProjectService;
import ru.evstigneev.tm.service.SessionService;
import ru.evstigneev.tm.service.TaskService;
import ru.evstigneev.tm.service.UserService;
import ru.evstigneev.tm.util.DateParser;

import javax.xml.ws.Endpoint;
import java.util.Scanner;

public class Bootstrap implements ServiceLocator {

    @NotNull
    private final Scanner scanner = new Scanner(System.in);
    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();
    @NotNull
    private final IProjectService projectService = new ProjectService(new ProjectRepository(), taskRepository);
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);
    @NotNull
    private final IUserRepository userRepository = new UserRepository();
    @NotNull
    private final IUserService userService = new UserService(userRepository);
    @NotNull
    private final ISessionService sessionService = new SessionService(new SessionRepository(), userRepository);
    @NotNull
    private final DateParser dateParser = new DateParser();
    @NotNull
    private IUserEndpoint userEndpoint = new UserEndpoint(userService, sessionService);
    @NotNull
    private IProjectEndpoint projectEndpoint = new ProjectEndpoint(projectService, sessionService);
    @NotNull
    private ITaskEndpoint taskEndpoint = new TaskEndpoint(taskService, projectService, sessionService);
    @NotNull
    private ISessionEndpoint sessionEndpoint = new SessionEndpoint(sessionService);
    @NotNull
    private IDomainEndpoint domainEndpoint = new DomainEndpoint(userService, projectService, taskService, sessionService);

    public void init() {
        System.out.println(Endpoint.publish(IUserEndpoint.URL, userEndpoint));
        System.out.println(Endpoint.publish(IProjectEndpoint.URL, projectEndpoint));
        System.out.println(Endpoint.publish(ITaskEndpoint.URL, taskEndpoint));
        System.out.println(Endpoint.publish(ISessionEndpoint.URL, sessionEndpoint));
        System.out.println(Endpoint.publish(IDomainEndpoint.URL, domainEndpoint));
    }

    @NotNull
    public Scanner getScanner() {
        return scanner;
    }

    @NotNull
    public ITaskRepository getTaskRepository() {
        return taskRepository;
    }

    @NotNull
    public IProjectService getProjectService() {
        return projectService;
    }

    @NotNull
    public ITaskService getTaskService() {
        return taskService;
    }

    @NotNull
    public IUserService getUserService() {
        return userService;
    }

    @NotNull
    public IUserEndpoint getUserEndpoint() {
        return userEndpoint;
    }

    public void setUserEndpoint(@NotNull final IUserEndpoint userEndpoint) {
        this.userEndpoint = userEndpoint;
    }

    @NotNull
    public IProjectEndpoint getProjectEndpoint() {
        return projectEndpoint;
    }

    public void setProjectEndpoint(@NotNull final IProjectEndpoint projectEndpoint) {
        this.projectEndpoint = projectEndpoint;
    }

    @NotNull
    public ITaskEndpoint getTaskEndpoint() {
        return taskEndpoint;
    }

    public void setTaskEndpoint(@NotNull final ITaskEndpoint taskEndpoint) {
        this.taskEndpoint = taskEndpoint;
    }

    @NotNull
    public DateParser getDateParser() {
        return dateParser;
    }

    @NotNull
    public ISessionEndpoint getSessionEndpoint() {
        return sessionEndpoint;
    }

    public void setSessionEndpoint(@NotNull final ISessionEndpoint sessionEndpoint) {
        this.sessionEndpoint = sessionEndpoint;
    }

    @NotNull
    public IDomainEndpoint getDomainEndpoint() {
        return domainEndpoint;
    }

    public void setDomainEndpoint(@NotNull final IDomainEndpoint domainEndpoint) {
        this.domainEndpoint = domainEndpoint;
    }

}
