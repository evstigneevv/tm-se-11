package ru.evstigneev.tm.comparator;

import org.jetbrains.annotations.NotNull;
import ru.evstigneev.tm.entity.Task;

import java.util.Comparator;

public class ComparatorTaskDateCreation implements Comparator<Task> {

    @Override
    public int compare(@NotNull final Task o1, @NotNull final Task o2) {
        return o1.getDateOfCreation().compareTo(o2.getDateOfCreation());
    }

}
