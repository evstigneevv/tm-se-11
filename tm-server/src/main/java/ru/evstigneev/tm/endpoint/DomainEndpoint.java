package ru.evstigneev.tm.endpoint;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.eclipse.persistence.jaxb.JAXBContextProperties;
import org.eclipse.persistence.jaxb.UnmarshallerProperties;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.evstigneev.tm.api.endpoint.IDomainEndpoint;
import ru.evstigneev.tm.api.service.IProjectService;
import ru.evstigneev.tm.api.service.ISessionService;
import ru.evstigneev.tm.api.service.ITaskService;
import ru.evstigneev.tm.api.service.IUserService;
import ru.evstigneev.tm.entity.Project;
import ru.evstigneev.tm.entity.Session;
import ru.evstigneev.tm.entity.Task;
import ru.evstigneev.tm.entity.User;
import ru.evstigneev.tm.wrapper.Projects;
import ru.evstigneev.tm.wrapper.Tasks;
import ru.evstigneev.tm.wrapper.Users;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

@WebService(endpointInterface = "ru.evstigneev.tm.api.endpoint.IDomainEndpoint")
public class DomainEndpoint implements IDomainEndpoint {

    @NotNull
    private IUserService userService;
    @NotNull
    private IProjectService projectService;
    @NotNull
    private ITaskService taskService;
    @NotNull
    private ISessionService sessionService;

    public DomainEndpoint() {
    }

    public DomainEndpoint(@NotNull final IUserService userService, @NotNull final IProjectService projectService,
                          @NotNull final ITaskService taskService, @NotNull final ISessionService sessionService) {
        this.userService = userService;
        this.projectService = projectService;
        this.taskService = taskService;
        this.sessionService = sessionService;
    }

    @Override
    @WebMethod
    public void writeDataJacksonJson(@WebParam(name = "session") @Nullable final Session session) throws Exception {
        sessionService.validate(session, session.getRoleType());
        @NotNull final ObjectMapper userMapper = new ObjectMapper();
        @NotNull final Users users = new Users();
        users.setUsers(userService.findAll());
        userMapper.writerWithDefaultPrettyPrinter().writeValue(new File("src/main/file/usersJackson.json"), users);
        @NotNull final ObjectMapper projectMapper = new ObjectMapper();
        @NotNull final Projects projects = new Projects();
        projects.setProjects(projectService.findAll());
        projectMapper.writerWithDefaultPrettyPrinter().writeValue(new File("src/main/file/projectsJackson.json"),
                projects);
        @NotNull final ObjectMapper taskMapper = new ObjectMapper();
        @NotNull final Tasks tasks = new Tasks();
        tasks.setTasks(taskService.findAll());
        taskMapper.writerWithDefaultPrettyPrinter().writeValue(new File("src/main/file/tasksJackson.json"),
                tasks);
    }

    @Override
    @WebMethod
    public void readDataJacksonJson(@WebParam(name = "session") @Nullable final Session session) throws Exception {
        sessionService.validate(session, session.getRoleType());
        @NotNull final ObjectMapper userMapper = new ObjectMapper();
        @NotNull final Users users = userMapper.readValue(new File("src/main/file/usersJackson.json"), Users.class);
        for (User user : users.getUsers()) {
            userService.persist(user.getId(), user);
        }
        @NotNull final ObjectMapper projectMapper = new ObjectMapper();
        @NotNull final Projects projects = projectMapper.readValue(new File("src/main/file/projectsJackson.json"), Projects.class);
        for (Project project : projects.getProjects()) {
            projectService.persist(project.getUserId(), project);
        }
        @NotNull final ObjectMapper taskMapper = new ObjectMapper();
        @NotNull final Tasks tasks = taskMapper.readValue(new File("src/main/file/tasksJackson.json"), Tasks.class);
        for (Task task : tasks.getTasks()) {
            taskService.persist(task.getUserId(), task);
        }
    }

    @Override
    @WebMethod
    public void readDataJacksonXml(@WebParam(name = "session") @Nullable final Session session) throws Exception {
        sessionService.validate(session, session.getRoleType());
        @NotNull final ObjectMapper userMapper = new XmlMapper();
        @NotNull final Users users = userMapper.readValue(new File("src/main/file/usersJackson.xml"), Users.class);
        for (User user : users.getUsers()) {
            userService.persist(user.getId(), user);
        }
        @NotNull final ObjectMapper projectMapper = new XmlMapper();
        @NotNull final Projects projects = projectMapper.readValue(new File("src/main/file/projectsJackson.xml"), Projects.class);
        for (Project project : projects.getProjects()) {
            projectService.persist(project.getUserId(), project);
        }
        @NotNull final ObjectMapper taskMapper = new XmlMapper();
        @NotNull final Tasks tasks = taskMapper.readValue(new File("src/main/file/tasksJackson.xml"), Tasks.class);
        for (Task task : tasks.getTasks()) {
            taskService.persist(task.getUserId(), task);
        }
    }

    @Override
    @WebMethod
    public void writeDataJacksonXml(@WebParam(name = "session") @Nullable final Session session) throws Exception {
        sessionService.validate(session, session.getRoleType());
        @NotNull final ObjectMapper userMapper = new XmlMapper();
        @NotNull final Users users = new Users();
        users.setUsers(userService.findAll());
        userMapper.writerWithDefaultPrettyPrinter().writeValue(new File("src/main/file/usersJackson.xml"), users);
        @NotNull final ObjectMapper projectMapper = new XmlMapper();
        @NotNull final Projects projects = new Projects();
        projects.setProjects(projectService.findAll());
        projectMapper.writerWithDefaultPrettyPrinter().writeValue(new File("src/main/file/projectsJackson.xml"),
                projects);
        @NotNull final ObjectMapper taskMapper = new XmlMapper();
        @NotNull final Tasks tasks = new Tasks();
        tasks.setTasks(taskService.findAll());
        taskMapper.writerWithDefaultPrettyPrinter().writeValue(new File("src/main/file/tasksJackson.xml"),
                tasks);
    }

    @Override
    @WebMethod
    public void readDataJaxbXml(@WebParam(name = "session") @Nullable final Session session) throws Exception {
        sessionService.validate(session, session.getRoleType());
        @NotNull final JAXBContext jaxbContextUser = JAXBContext.newInstance(Users.class);
        @NotNull final Unmarshaller unmarshallerUser = jaxbContextUser.createUnmarshaller();
        @NotNull final Users users = (Users) unmarshallerUser.unmarshal(new File("src/main/file/users.xml"));
        for (User user : users.getUsers()) {
            userService.persist(user.getId(), user);
        }
        @NotNull final JAXBContext jaxbContextProject = JAXBContext.newInstance(Projects.class);
        @NotNull final Unmarshaller unmarshallerProject = jaxbContextProject.createUnmarshaller();
        @NotNull final Projects projects = (Projects) unmarshallerProject.unmarshal(new File("src/main/file/projects" +
                ".xml"));
        for (Project project : projects.getProjects()) {
            projectService.persist(project.getUserId(), project);
        }
        @NotNull final JAXBContext jaxbContextTask = JAXBContext.newInstance(Tasks.class);
        @NotNull final Unmarshaller unmarshallerTask = jaxbContextTask.createUnmarshaller();
        @NotNull final Tasks tasks = (Tasks) unmarshallerTask.unmarshal(new File("src/main/file/tasks.xml"));
        for (Task task : tasks.getTasks()) {
            taskService.persist(task.getUserId(), task);
        }
    }

    @Override
    @WebMethod
    public void writeDataJaxbXml(@WebParam(name = "session") @Nullable final Session session) throws Exception {
        sessionService.validate(session, session.getRoleType());
        @NotNull final JAXBContext jaxbContextUser = JAXBContext.newInstance(Users.class);
        @NotNull final Marshaller marshallerUser = jaxbContextUser.createMarshaller();
        marshallerUser.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        @NotNull final File fileUsers = new File("src/main/file/users.xml");
        @NotNull final Users users = new Users();
        users.setUsers(userService.findAll());
        marshallerUser.marshal(users, fileUsers);
        @NotNull final JAXBContext jaxbContextProject = JAXBContext.newInstance(Projects.class);
        @NotNull final Marshaller marshallerProject = jaxbContextProject.createMarshaller();
        marshallerProject.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        @NotNull final File fileProjects = new File("src/main/file/projects.xml");
        @NotNull final Projects projects = new Projects();
        projects.setProjects(projectService.findAll());
        marshallerProject.marshal(projects, fileProjects);
        @NotNull final JAXBContext jaxbContextTask = JAXBContext.newInstance(Tasks.class);
        @NotNull final Marshaller marshallerTask = jaxbContextTask.createMarshaller();
        marshallerTask.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        @NotNull final File fileTasks = new File("src/main/file/tasks.xml");
        @NotNull final Tasks tasks = new Tasks();
        tasks.setTasks(taskService.findAll());
        marshallerTask.marshal(tasks, fileTasks);
    }

    @Override
    @WebMethod
    public void readDataJaxbJson(@WebParam(name = "session") @Nullable final Session session) throws Exception {
        sessionService.validate(session, session.getRoleType());
        @NotNull final JAXBContext jaxbContextUsers = JAXBContext.newInstance(Users.class);
        @NotNull final Unmarshaller unmarshallerUsers = jaxbContextUsers.createUnmarshaller();
        unmarshallerUsers.setProperty(UnmarshallerProperties.MEDIA_TYPE, "application/json");
        unmarshallerUsers.setProperty(UnmarshallerProperties.JSON_INCLUDE_ROOT, true);
        @Nullable final Users users = (Users) unmarshallerUsers.unmarshal(new File("src/main/file/users.json"));
        if (users != null) {
            for (User user : users.getUsers()) {
                userService.persist(user.getId(), user);
            }
        }
        @NotNull final JAXBContext jaxbContextProjects = JAXBContext.newInstance(Projects.class);
        @NotNull final Unmarshaller unmarshallerProjects = jaxbContextProjects.createUnmarshaller();
        unmarshallerProjects.setProperty(UnmarshallerProperties.MEDIA_TYPE, "application/json");
        unmarshallerProjects.setProperty(UnmarshallerProperties.JSON_INCLUDE_ROOT, true);
        @Nullable final Projects projects = (Projects) unmarshallerProjects.unmarshal(new File("src/main/file" +
                "/projects.json"));
        if (projects != null) {
            for (Project project : projects.getProjects()) {
                projectService.persist(project.getUserId(), project);
            }
        }
        @NotNull final JAXBContext jaxbContextTasks = JAXBContext.newInstance(Tasks.class);
        @NotNull final Unmarshaller unmarshallerTasks = jaxbContextTasks.createUnmarshaller();
        unmarshallerTasks.setProperty(UnmarshallerProperties.MEDIA_TYPE, "application/json");
        unmarshallerTasks.setProperty(UnmarshallerProperties.JSON_INCLUDE_ROOT, true);
        @Nullable final Tasks tasks = (Tasks) unmarshallerTasks.unmarshal(new File("src/main/file/tasks.json"));
        if (tasks != null) {
            for (Task task : tasks.getTasks()) {
                taskService.persist(task.getUserId(), task);
            }
        }
    }

    @Override
    @WebMethod
    public void writeDataJaxbJson(@WebParam(name = "session") @Nullable final Session session) throws Exception {
        sessionService.validate(session, session.getRoleType());
        @NotNull final JAXBContext jaxbContextUsers = JAXBContext.newInstance(Users.class);
        @NotNull final Marshaller marshallerUser = jaxbContextUsers.createMarshaller();
        marshallerUser.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshallerUser.setProperty(JAXBContextProperties.JSON_INCLUDE_ROOT, true);
        marshallerUser.setProperty(JAXBContextProperties.MEDIA_TYPE, "application/json");
        @NotNull final File fileUsers = new File("src/main/file/users.json");
        @NotNull final Users users = new Users();
        users.setUsers(userService.findAll());
        marshallerUser.marshal(users, fileUsers);
        @NotNull final JAXBContext jaxbContextProjects = JAXBContext.newInstance(Projects.class);
        @NotNull final Marshaller marshallerProjects = jaxbContextProjects.createMarshaller();
        marshallerProjects.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshallerProjects.setProperty(JAXBContextProperties.JSON_INCLUDE_ROOT, true);
        marshallerProjects.setProperty(JAXBContextProperties.MEDIA_TYPE, "application/json");
        @NotNull final File fileProjects = new File("src/main/file/projects.json");
        @NotNull final Projects projects = new Projects();
        projects.setProjects(projectService.findAll());
        marshallerProjects.marshal(projects, fileProjects);
        @NotNull final JAXBContext jaxbContextTasks = JAXBContext.newInstance(Tasks.class);
        @NotNull final Marshaller marshallerTasks = jaxbContextTasks.createMarshaller();
        marshallerTasks.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshallerTasks.setProperty(JAXBContextProperties.JSON_INCLUDE_ROOT, true);
        marshallerTasks.setProperty(JAXBContextProperties.MEDIA_TYPE, "application/json");
        @NotNull final File fileTasks = new File("src/main/file/tasks.json");
        @NotNull final Tasks tasks = new Tasks();
        tasks.setTasks(taskService.findAll());
        marshallerTasks.marshal(tasks, fileTasks);
    }

    @Override
    @WebMethod
    public void writeDataSerialization(@WebParam(name = "session") @Nullable final Session session) throws Exception {
        sessionService.validate(session, session.getRoleType());
        @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("src/main/file/data.ser"));
        @Nullable final List<User> userList = new ArrayList<>(userService.findAll());
        @Nullable final List<Project> projectList = new ArrayList<>(projectService.findAll());
        @Nullable final List<Task> taskList = new ArrayList<>(taskService.findAll());
        objectOutputStream.writeObject(userList);
        objectOutputStream.writeObject(projectList);
        objectOutputStream.writeObject(taskList);
        objectOutputStream.close();
    }

    @Override
    @WebMethod
    public void readDataSerialization(@WebParam(name = "session") @Nullable final Session session) throws Exception {
        sessionService.validate(session, session.getRoleType());
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream("src/main/file/data.ser"));
        @Nullable final List<User> users = (List<User>) objectInputStream.readObject();
        @Nullable final List<Project> projects = (List<Project>) objectInputStream.readObject();
        @Nullable final List<Task> tasks = (List<Task>) objectInputStream.readObject();
        if (users != null) {
            for (User user : users) {
                userService.persist(user.getId(), user);
            }
        }
        if (projects != null) {
            for (Project project : projects) {
                @NotNull final String userId = project.getUserId();
                projectService.persist(userId, project);
            }
        }
        if (tasks != null) {
            for (Task task : tasks) {
                @NotNull final String userId = task.getUserId();
                taskService.persist(userId, task);
            }
        }
        objectInputStream.close();
    }

}
