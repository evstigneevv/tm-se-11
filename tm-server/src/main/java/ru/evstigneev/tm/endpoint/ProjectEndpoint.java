package ru.evstigneev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.evstigneev.tm.api.endpoint.IProjectEndpoint;
import ru.evstigneev.tm.api.service.IProjectService;
import ru.evstigneev.tm.api.service.ISessionService;
import ru.evstigneev.tm.entity.Project;
import ru.evstigneev.tm.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;
import java.util.List;

@WebService(endpointInterface = "ru.evstigneev.tm.api.endpoint.IProjectEndpoint")
public class ProjectEndpoint implements IProjectEndpoint {

    @NotNull
    private IProjectService projectService;
    @NotNull
    private ISessionService sessionService;

    public ProjectEndpoint() {
    }

    public ProjectEndpoint(@NotNull final IProjectService projectService, @NotNull final ISessionService sessionService) {
        this.projectService = projectService;
        this.sessionService = sessionService;
    }

    @Override
    @WebMethod
    public Collection<Project> findAllProjects(@WebParam(name = "session") @Nullable final Session session) throws Exception {
        sessionService.validate(session, session.getRoleType());
        return projectService.findAll();
    }

    @Override
    @WebMethod
    public Project createProject(@WebParam(name = "session") @Nullable final Session session,
                                 @WebParam(name = "projectName") @NotNull final String projectName) throws Exception {
        sessionService.validate(session);
        return projectService.create(session.getUserId(), projectName);
    }

    @Override
    @WebMethod
    public boolean removeProject(@WebParam(name = "session") @Nullable final Session session,
                                 @WebParam(name = "projectId") @NotNull final String projectId) throws Exception {
        sessionService.validate(session);
        return projectService.remove(session.getUserId(), projectId);
    }

    @Override
    @WebMethod
    public Project updateProject(@WebParam(name = "session") @Nullable final Session session,
                                 @WebParam(name = "projectId") @NotNull final String projectId,
                                 @WebParam(name = "newProjectName") @NotNull final String newProjectName) throws Exception {
        sessionService.validate(session);
        return projectService.update(session.getUserId(), projectId, newProjectName);
    }

    @Override
    @WebMethod
    public Project findOneProject(@WebParam(name = "session") @Nullable final Session session,
                                  @WebParam(name = "projectId") @NotNull final String projectId) throws Exception {
        sessionService.validate(session);
        return projectService.findOne(session.getUserId(), projectId);
    }

    @Override
    @WebMethod
    public Project mergeProject(@WebParam(name = "session") @Nullable final Session session,
                                @WebParam(name = "project") @NotNull final Project project) throws Exception {
        sessionService.validate(session);
        return projectService.merge(session.getUserId(), project);
    }

    @Override
    @WebMethod
    public Project persistProject(@WebParam(name = "session") @Nullable final Session session,
                                  @WebParam(name = "project") @NotNull final Project project) throws Exception {
        sessionService.validate(session);
        return projectService.persist(session.getUserId(), project);
    }

    @Override
    @WebMethod
    public Collection<Project> findAllProjectsByUserId(@WebParam(name = "session") @Nullable final Session session) throws Exception {
        sessionService.validate(session);
        return projectService.findAllByUserId(session.getUserId());
    }

    @Override
    @WebMethod
    public void removeAllProjects(@WebParam(name = "session") @Nullable final Session session) throws Exception {
        sessionService.validate(session, session.getRoleType());
        projectService.removeAll();
    }

    @Override
    @WebMethod
    public boolean removeAllProjectsByUserId(@WebParam(name = "session") @Nullable final Session session) throws Exception {
        sessionService.validate(session);
        return projectService.removeAllByUserId(session.getUserId());
    }

    @Override
    @WebMethod
    public List<Project> sortProjects(@WebParam(name = "session") @Nullable final Session session,
                                      @WebParam(name = "comparatorName") @NotNull final String comparatorName) throws Exception {
        sessionService.validate(session);
        return projectService.sort(comparatorName);
    }

    @Override
    @WebMethod
    public List<Project> searchProjectByString(@WebParam(name = "session") @Nullable final Session session,
                                               @WebParam(name = "string") @NotNull final String string) throws Exception {
        sessionService.validate(session);
        return projectService.searchByString(string);
    }

}
