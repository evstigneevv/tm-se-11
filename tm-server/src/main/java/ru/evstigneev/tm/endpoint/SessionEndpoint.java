package ru.evstigneev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.evstigneev.tm.api.endpoint.ISessionEndpoint;
import ru.evstigneev.tm.api.service.ISessionService;
import ru.evstigneev.tm.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;
import java.util.List;

@WebService(endpointInterface = "ru.evstigneev.tm.api.endpoint.ISessionEndpoint")
public class SessionEndpoint implements ISessionEndpoint {

    @NotNull
    private ISessionService sessionService;

    public SessionEndpoint() {
    }

    public SessionEndpoint(@NotNull final ISessionService sessionService) {
        this.sessionService = sessionService;
    }

    @Override
    @WebMethod
    public void validate(@WebParam(name = "session") @NotNull final Session session) throws Exception {
        sessionService.validate(session);
    }

    @Override
    @WebMethod
    public Session openSession(@WebParam(name = "login") @NotNull final String login,
                               @WebParam(name = "password") @NotNull final String password) throws Exception {
        return sessionService.openSession(login, password);
    }

    @Override
    @WebMethod
    public boolean closeSession(@WebParam(name = "session") @NotNull final Session session) throws Exception {
        sessionService.validate(session);
        return sessionService.closeSession(session);
    }

    @Override
    @WebMethod
    public Session merge(@WebParam(name = "session") @NotNull final Session session) throws Exception {
        sessionService.validate(session);
        return sessionService.merge(session.getUserId(), session);
    }

    @Override
    @WebMethod
    public Session persist(@WebParam(name = "session") @NotNull final Session session) throws Exception {
        sessionService.validate(session);
        return sessionService.persist(session.getUserId(), session);
    }

    @Override
    @WebMethod
    public void removeAll(@WebParam(name = "session") @NotNull final Session session) throws Exception {
        sessionService.validate(session, session.getRoleType());
        sessionService.removeAll();
    }

    @Override
    @WebMethod
    public boolean removeByUserId(@WebParam(name = "session") @NotNull final Session session) throws Exception {
        sessionService.validate(session);
        return sessionService.removeByUserId(session.getUserId());
    }

    @Override
    @WebMethod
    public Session findSessionByUserId(@WebParam(name = "session") @NotNull final Session session) throws Exception {
        sessionService.validate(session);
        return sessionService.findSessionByUserId(session.getUserId());
    }

    @Override
    @WebMethod
    public Collection<Session> findAll(@WebParam(name = "session") @NotNull final Session session) throws Exception {
        sessionService.validate(session, session.getRoleType());
        return sessionService.findAll();
    }

    @Override
    @WebMethod
    public List<String> getBlockedSignatureList(@WebParam(name = "session") @NotNull final Session session) throws Exception {
        sessionService.validate(session, session.getRoleType());
        return sessionService.getBlockedSignatureList();
    }

    @Override
    @WebMethod
    public void setBlockedSignatureList(@WebParam(name = "session") @NotNull final Session session,
                                        @WebParam(name = "list") @NotNull final List<String> blockedSignatureList) throws Exception {
        sessionService.validate(session, session.getRoleType());
        sessionService.setBlockedSignatureList(blockedSignatureList);
    }

    @Override
    @WebMethod
    public boolean addSessionToBlockedList(@WebParam(name = "session") @NotNull final Session session,
                                           @WebParam(name = "signature") @NotNull final String signature) throws Exception {
        sessionService.validate(session, session.getRoleType());
        return sessionService.addSessionToBlockedList(signature);
    }

}
