package ru.evstigneev.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.evstigneev.tm.enumerated.Status;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class Task implements Comparable<Task>, Serializable {

    @NotNull
    private Date dateOfCreation = new Date();
    @NotNull
    private String name;
    @NotNull
    private String id;
    @NotNull
    private String projectId;
    @Nullable
    private String description;
    @Nullable
    private Date dateStart;
    @Nullable
    private Date dateFinish;
    @NotNull
    private String userId;
    @NotNull
    private Status status = Status.PLANNING;

    @Override
    public String toString() {
        return "User ID: " + getUserId() + " | Project ID:" + getProjectId() + " | Task ID:" + getId() + " | Task name: "
                + getName() + " | Task description: " + getDescription() + " | Date of creation: "
                + getDateOfCreation() + " | Date of start: " + getDateStart() + " | Date of finish: "
                + getDateFinish() + " | Task status: " + getStatus();
    }

    @Override
    public int compareTo(@NotNull final Task task) {
        return this.getStatus().compareTo(task.getStatus());
    }

}

