package ru.evstigneev.tm.exception;

public class EmptyStringException extends Exception {

    public EmptyStringException() {
        super("Entered an empty string!");
    }

    public EmptyStringException(String message) {
        super(message);
    }

}
