package ru.evstigneev.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.evstigneev.tm.exception.EmptyStringException;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public abstract class AbstractRepository<V> {

    @NotNull
    protected final Map<String, V> entities = new LinkedHashMap<>();

    public abstract V merge(@NotNull final String userId, @NotNull final V value) throws EmptyStringException;

    public abstract V persist(@NotNull final String userId, @NotNull final V value) throws EmptyStringException;

    public abstract void removeAll();

    public abstract Collection<V> findAll();

}
