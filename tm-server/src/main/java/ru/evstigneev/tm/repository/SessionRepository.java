package ru.evstigneev.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.evstigneev.tm.api.repository.ISessionRepository;
import ru.evstigneev.tm.entity.Session;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

    @NotNull
    private List<String> blockedSignatureList = new ArrayList<>();

    @Override
    public Session merge(@NotNull final String userId, @NotNull final Session session) {
        return persist(userId, session);
    }

    @Override
    public Session persist(@NotNull final String userId, @NotNull final Session session) {
        return entities.put(userId, session);
    }

    @Override
    public void removeAll() {
        entities.clear();
    }

    @Override
    public boolean removeByUserId(@NotNull final String userId) {
        return false;
    }

    @Override
    public Session findSessionByUserId(@NotNull final String userId) {
        return entities.get(userId);
    }

    @Override
    public Collection<Session> findAll() {
        return entities.values();
    }

    @NotNull
    @Override
    public List<String> getBlockedSignatureList() {
        return blockedSignatureList;
    }

    @Override
    public void setBlockedSignatureList(@NotNull final List<String> blockedSignatureList) {
        this.blockedSignatureList = blockedSignatureList;
    }

    @Override
    public boolean addSessionToBlockedList(@NotNull final String signature) {
        return blockedSignatureList.add(signature);
    }

    public boolean addSession(@NotNull final String id, @NotNull final Session session) {
        return entities.put(id, session) != null;
    }

}
