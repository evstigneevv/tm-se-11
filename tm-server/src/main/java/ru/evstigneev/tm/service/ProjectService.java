package ru.evstigneev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.evstigneev.tm.api.repository.IProjectRepository;
import ru.evstigneev.tm.api.repository.ITaskRepository;
import ru.evstigneev.tm.api.service.IProjectService;
import ru.evstigneev.tm.comparator.ComparatorProjectDateCreation;
import ru.evstigneev.tm.comparator.ComparatorProjectDateFinish;
import ru.evstigneev.tm.comparator.ComparatorProjectDateStart;
import ru.evstigneev.tm.entity.Project;
import ru.evstigneev.tm.exception.CommandCorruptException;
import ru.evstigneev.tm.exception.EmptyStringException;
import ru.evstigneev.tm.exception.RepositoryException;

import java.util.*;

public class ProjectService extends AbstractService<Project> implements IProjectService {

    private IProjectRepository projectRepository;
    private ITaskRepository taskRepository;

    public ProjectService(final IProjectRepository projectRepository, final ITaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public Collection<Project> findAll() throws Exception {
        if (projectRepository.findAll().isEmpty()) {
            throw new RepositoryException("No projects found!");
        }
        return projectRepository.findAll();
    }

    @Override
    public Project create(@NotNull final String userId, @NotNull final String projectName) throws EmptyStringException {
        if (userId.isEmpty() || projectName.isEmpty()) {
            throw new EmptyStringException();
        }
        return projectRepository.create(userId, projectName);
    }

    @Override
    public boolean remove(@NotNull final String userId, @NotNull final String projectId) throws EmptyStringException {
        if (userId.isEmpty() || projectId.isEmpty()) {
            throw new EmptyStringException();
        }
        taskRepository.deleteAllProjectTasks(userId, projectId);
        return projectRepository.remove(userId, projectId);
    }

    @Override
    public Project update(@NotNull final String userId, @NotNull final String projectId, @NotNull final String newProjectName) throws EmptyStringException {
        if (userId.isEmpty() || projectId.isEmpty() || newProjectName.isEmpty()) {
            throw new EmptyStringException();
        }
        return projectRepository.update(userId, projectId, newProjectName);
    }

    @Override
    public Project findOne(@NotNull final String userId, @NotNull final String projectId) throws EmptyStringException {
        if (userId.isEmpty() || projectId.isEmpty()) {
            throw new EmptyStringException();
        }
        return projectRepository.findOne(userId, projectId);
    }

    @Override
    public Project merge(@NotNull final String userId, @NotNull final Project project) throws Exception {
        if (userId.isEmpty()) {
            throw new EmptyStringException();
        }
        return projectRepository.merge(userId, project);
    }

    @Override
    public Project persist(@NotNull final String userId, @NotNull final Project project) throws Exception {
        if (userId.isEmpty()) {
            throw new EmptyStringException();
        }
        return projectRepository.persist(userId, project);
    }

    @Override
    public Collection<Project> findAllByUserId(@NotNull final String userId) throws EmptyStringException {
        if (userId.isEmpty()) {
            throw new EmptyStringException();
        }
        return projectRepository.findAllByUserId(userId);
    }

    @Override
    public void removeAll() {
        projectRepository.removeAll();
    }

    @Override
    public boolean removeAllByUserId(@NotNull final String userId) throws EmptyStringException {
        if (userId.isEmpty()) {
            throw new EmptyStringException();
        }
        taskRepository.removeAllByUserId(userId);
        return projectRepository.removeAllByUserId(userId);
    }

    public List<Project> sort(@NotNull final String comparatorName) throws Exception {
        @NotNull final List<Project> sortedList = new ArrayList<>(findAll());
        @Nullable Comparator<Project> comparator = findComparator(comparatorName);
        if (comparator == null) {
            Collections.sort(sortedList);
            return sortedList;
        }
        Collections.sort(sortedList, comparator);
        return sortedList;
    }

    private Comparator<Project> findComparator(@NotNull final String name) throws CommandCorruptException {
        switch (name.toLowerCase()) {
            default:
                throw new CommandCorruptException();
            case "date start": {
                return new ComparatorProjectDateStart();
            }
            case "date finish": {
                return new ComparatorProjectDateFinish();
            }
            case "date creation": {
                return new ComparatorProjectDateCreation();
            }
            case "status": {
                return null;
            }
        }
    }

    public List<Project> searchByString(@NotNull final String string) throws Exception {
        if (string.isEmpty()) {
            throw new EmptyStringException();
        }
        @NotNull List<Project> projectList = new ArrayList<>();
        for (Project project : findAll()) {
            if (project.getName().contains(string) || (project.getDescription() != null && project.getDescription().contains(string))) {
                projectList.add(project);
            }
        }
        return projectList;
    }

}
