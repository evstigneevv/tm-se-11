package ru.evstigneev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.evstigneev.tm.api.repository.ISessionRepository;
import ru.evstigneev.tm.api.repository.IUserRepository;
import ru.evstigneev.tm.api.service.ISessionService;
import ru.evstigneev.tm.entity.Session;
import ru.evstigneev.tm.entity.User;
import ru.evstigneev.tm.enumerated.RoleType;
import ru.evstigneev.tm.exception.EmptyStringException;
import ru.evstigneev.tm.exception.NoPermissionException;
import ru.evstigneev.tm.util.SignatureCalculator;

import java.util.Collection;
import java.util.List;
import java.util.UUID;

public class SessionService implements ISessionService {

    private static final long SESSION_EXPIRATION_TIME = 1_800_000L;
    @NotNull
    private IUserRepository userRepository;
    @NotNull
    private ISessionRepository sessionRepository;

    public SessionService() {
    }

    public SessionService(@NotNull final ISessionRepository sessionRepository,
                          @NotNull final IUserRepository userRepository) {
        this.userRepository = userRepository;
        this.sessionRepository = sessionRepository;
    }

    @Override
    public void validate(@Nullable final Session session) throws Exception {
        if (session == null) {
            throw new NoPermissionException();
        }
        if (session.getSignature() == null || session.getId().isEmpty() || session.getUserId().isEmpty()
                || session.getRoleType().displayName().isEmpty() || session.getSignature().isEmpty()) {
            throw new NoPermissionException();
        }
        if (sessionRepository.getBlockedSignatureList().contains(session.getSignature())) {
            throw new NoPermissionException();
        }
        if (System.currentTimeMillis() - session.getTimestamp() > SESSION_EXPIRATION_TIME) {
            throw new NoPermissionException();
        }
        @NotNull final Session temp = session.clone();
        temp.setSignature(null);
        temp.setSignature(SignatureCalculator.calculateSignature(temp));
        if (temp.getSignature() != null) {
            if (!temp.getSignature().equals(session.getSignature())) {
                throw new NoPermissionException();
            }
        }
    }

    @Override
    public void validate(@Nullable final Session session, @NotNull final RoleType roleType) throws Exception {
        validate(session);
        if (!roleType.equals(RoleType.ADMIN)) {
            throw new NoPermissionException();
        }
    }

    @Override
    public Session openSession(@NotNull final String login, @NotNull final String password) throws Exception {
        if (login.isEmpty() || password.isEmpty()) {
            throw new EmptyStringException();
        }
        @NotNull final Session session = new Session();
        @NotNull final User user = userRepository.findByLogin(login);
        session.setUserId(user.getId());
        session.setId(UUID.randomUUID().toString());
        session.setRoleType(user.getRole());
        session.setTimestamp(System.currentTimeMillis());
        session.setSignature(SignatureCalculator.calculateSignature(session));
        sessionRepository.addSession(session.getId(), session);
        return session;
    }

    @Override
    public boolean closeSession(@NotNull final Session session) {
        return sessionRepository.removeByUserId(session.getUserId());
    }

    @Override
    public Session merge(@NotNull final String userId, @NotNull final Session session) throws EmptyStringException {
        return sessionRepository.merge(userId, session);
    }

    @Override
    public Session persist(@NotNull final String userId, @NotNull final Session session) throws EmptyStringException {
        return sessionRepository.persist(userId, session);
    }

    @Override
    public void removeAll() {
        sessionRepository.removeAll();
    }

    @Override
    public boolean removeByUserId(@NotNull final String userId) {
        return sessionRepository.removeByUserId(userId);
    }

    @Override
    public Session findSessionByUserId(@NotNull final String userId) {
        return sessionRepository.findSessionByUserId(userId);
    }

    @Override
    public Collection<Session> findAll() {
        return sessionRepository.findAll();
    }

    @Override
    public List<String> getBlockedSignatureList() {
        return sessionRepository.getBlockedSignatureList();
    }

    @Override
    public void setBlockedSignatureList(@NotNull final List<String> blockedSignatureList) {
        sessionRepository.setBlockedSignatureList(blockedSignatureList);
    }

    @Override
    public boolean addSessionToBlockedList(@NotNull final String signature) {
        return sessionRepository.addSessionToBlockedList(signature);
    }

}