package ru.evstigneev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.evstigneev.tm.api.repository.IUserRepository;
import ru.evstigneev.tm.api.service.IUserService;
import ru.evstigneev.tm.entity.User;
import ru.evstigneev.tm.enumerated.RoleType;
import ru.evstigneev.tm.exception.EmptyStringException;
import ru.evstigneev.tm.util.PasswordParser;

import java.util.Collection;

public class UserService extends AbstractService<User> implements IUserService {

    private IUserRepository userRepository;

    public UserService(@NotNull final IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User create(@NotNull final String login, @NotNull final String password) throws EmptyStringException {
        if (login.isEmpty() || password.isEmpty()) {
            throw new EmptyStringException();
        }
        return userRepository.create(login, PasswordParser.getPasswordHash(password));
    }

    @Override
    public User create(@NotNull final String login, @NotNull final String password, @NotNull final RoleType role) throws EmptyStringException {
        if (login.isEmpty() || password.isEmpty()) {
            throw new EmptyStringException();
        }
        return userRepository.create(login, password, role);
    }

    @Override
    public Collection<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public boolean checkPassword(@NotNull final String login, @NotNull final String password) throws EmptyStringException {
        if (login.isEmpty() || password.isEmpty()) {
            throw new EmptyStringException();
        }
        @Nullable Collection<User> users = findAll();
        if (users != null) {
            for (User user : users) {
                if (user.getLogin().equals(login)) {
                    return PasswordParser.getPasswordHash(password).equals(user.getPasswordHash());
                }
            }
        }
        return false;
    }

    @Override
    public boolean checkPasswordByUserId(@NotNull final String userId, @NotNull final String password) throws Exception {
        if (userId.isEmpty() || password.isEmpty()) {
            throw new EmptyStringException();
        }
        @Nullable Collection<User> users = findAll();
        if (users != null) {
            for (User user : users) {
                if (user.getId().equals(userId)) {
                    return PasswordParser.getPasswordHash(password).equals(user.getPasswordHash());
                }
            }
        }
        return false;
    }

    @Override
    public boolean updatePassword(@NotNull final String userId, @NotNull final String newPassword) throws EmptyStringException {
        if (userId.isEmpty() || newPassword.isEmpty()) {
            throw new EmptyStringException();
        }
        return userRepository.updatePassword(userId, newPassword);
    }

    @Override
    public boolean isAdmin(@NotNull final User user) {
        return user.getRole().equals(RoleType.ADMIN);
    }

    @Override
    public User findByLogin(@NotNull final String login) throws EmptyStringException {
        if (login.isEmpty()) {
            throw new EmptyStringException();
        }
        return userRepository.findByLogin(login);
    }

    @Override
    public void update(@NotNull final String userId, @NotNull final String userName) throws EmptyStringException {
        if (userId.isEmpty() || userName.isEmpty()) {
            throw new EmptyStringException();
        }
        userRepository.update(userId, userName);
    }

    @Override
    public boolean remove(@NotNull final String userId) throws EmptyStringException {
        if (userId.isEmpty()) {
            throw new EmptyStringException();
        }
        return userRepository.remove(userId);
    }

    @Override
    public User persist(@NotNull final String userId, @NotNull final User user) throws EmptyStringException {
        if (userId.isEmpty()) {
            throw new EmptyStringException();
        }
        return userRepository.persist(userId, user);
    }

    @Override
    public void removeAll() {
        userRepository.removeAll();
    }

    @Override
    public User merge(@NotNull final String userId, @NotNull final User user) throws EmptyStringException {
        if (userId.isEmpty()) {
            throw new EmptyStringException();
        }
        return userRepository.merge(userId, user);
    }

}
